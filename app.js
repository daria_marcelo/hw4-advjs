// AJAX використовується для асинхронного завантаження даних з сервера і оновлення частини веб-сторінки, що дозволяє створювати більш динамічні та ефективні веб-додатки. Використання AJAX дозволяє зменшити час очікування на відповідь сервера, а також зменшити кількість передач даних між клієнтом та сервером, оскільки завантажуються тільки необхідні дані. Це робить веб-додатки швидшими та зручнішими для користувачів.

const api = "https://ajax.test-danit.com/api/swapi/films";

sendRequest(api)
    .then(response => response.json())
    .then(data => {
        renderMovies(data);
        return data;
    })
    .catch(error => console.error(error));


function sendRequest(url) {
    return fetch(url);
}

function renderMovies(data) {
    data.forEach(({ characters, episodeId, name, openingCrawl }) => {
        const containerMovie = document.createElement('div');
        containerMovie.innerHTML = `
        <h2>Episode ${episodeId}: ${name}</h2>
        <div class="characters-loader"><span class="loader"></span></div>
        <ul class="characters-list"></ul>
        <p>${openingCrawl}</p>
      `;
        document.body.append(containerMovie);

        showCharacters()

        function showCharacters() {
            const charactersList = containerMovie.querySelector('.characters-list');
            const loaderContainer = containerMovie.querySelector('.characters-loader');

            Promise.all(characters.map(url => fetch(url)
                .then(response => response.json())))
                .then(characters => {
                    charactersList.innerHTML = characters.map(character => `<li>${character.name}</li>`).join('');
                    loaderContainer.style.display = 'none';
                })

                .catch(error => {
                    console.error(error);
                    charactersList.innerHTML = '<li>Error loading characters</li>';
                    loaderContainer.style.display = 'none';
                });
        }
    });
}


